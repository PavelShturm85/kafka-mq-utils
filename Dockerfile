FROM python:3.8.8

COPY . .

RUN pipenv install --dev --system --deploy --ignore-pipfile &&\
    arti-pip -a end
