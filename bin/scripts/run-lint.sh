#!/usr/bin/env bash

pylint kafka_mq_utils
mypy --config-file mypy.ini .
