MQ utils package for chat
==
This package encapsulates all logic around mq messaging and contains generic consumer, producer and mocks for them.  
Also it has abstract subclass of generic consumer/producer for exact MQ realisation:
* Kafka

Core usage case — subclass any of those realisations, override 1 method and get job done.

Quickstart:
===
### Consumer
1. Subclass consumer:
    ```python
    class MyFunnyConsumer(AsyncBaseKafkaConsumer):
        encoder = SuperPydanticSchema  # also can be "text", "bytes" or "json"
        async def process_one_message(message):
            await database_connection.store({'json': message})
    ```
1. How to use subclassed:
    1. Minimal
        ```python
        consumer = MyFunnyConsumer().config_driver('my_topic', bootstrap_servers='localhost:9092')
        await consumer.start()
        ```
    1. Medium
        ```python
        consumer = MyFunnyConsumer().config_driver(
            'my_topic', 'my_other_topic',
            bootstrap_servers='localhost:9092',
            group_id='my-group'
        )
        await consumer.start()
        ```
    1. Hard https://github.com/aio-libs/aiokafka/blob/master/aiokafka/consumer/consumer.py
​
### Producer
1. Basic example:
    ```python
    producer = AsyncKafkaProducer().config_driver(
            bootstrap_servers='localhost:9092', enable_idempotence=True, compression_type='gzip'
        )
    await producer.init_connections()
    await producer.send_json(message, 'topic_name')
    await producer.close_connections()
    ```
1. Example with context manager:
    ```python
    async with AsyncKafkaProducer().config_driver(
        bootstrap_servers='localhost:9092', enable_idempotence=True, compression_type="gzip"
    ) as produce:
        await produce.send_json(message, 'test_topic')
    ```

### Mock
```python
import pytest

from .mock import AIOKafkaConsumerMock


@pytest.mark.asyncio
@pytest.mark.parametrize(
    'message', [{"jwt": "some_jwt", "session_id": "some_id", "payload": {}}, {"test": "test"}],
)
async def test_kafka_consumer(monkeypatch, message):
    class CustomAIOKafkaConsumerMock(AIOKafkaConsumerMock):
        def __init__(self, *_, **__):
            super().__init__(_, __)
            self.value = message
            self.counter: int = 1
            self.iter_to: int = 3

    monkeypatch.setattr("aiokafka.AIOKafkaConsumer", CustomAIOKafkaConsumerMock)
```

### Consumer middleware
1. you can add middleware to the consumer by passing middleware list to the init parameters
      ```python
      consumer = MyFancyConsumer(middleware=[MyFancyMiddleware()])
      ```
1. middleware is just a callable which process original message and return it back (maybe modified)
1. middleware calls before each message will be processed
