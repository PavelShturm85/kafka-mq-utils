"""Package setup."""
import setuptools


with open('README.md', 'r') as fh:
    long_description = fh.read()


setuptools.setup(
    name='kafka-mq-utils',
    version='1.0',
    author='',
    author_email='',
    description='Utils package for mq',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='',
    packages=setuptools.find_packages(),
    classifiers=['Programming Language :: Python :: 3.8'],
    python_requires='>=3.8',
    install_requires=['aiokafka', 'tenacity', 'pydantic', 'orjson', 'loguru'],
)
