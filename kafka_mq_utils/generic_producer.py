"""Base kafka producer."""
from __future__ import annotations
import typing

import aiokafka
import orjson as json
from aiokafka.errors import KafkaError, KafkaTimeoutError
from loguru import logger


class AsyncKafkaProducer:
    """Producer for kafka."""

    producer: aiokafka.AIOKafkaProducer
    _params: typing.Dict

    async def __aenter__(self):
        """Open connect with kafka producer."""
        await self.init_connections()
        return self

    async def __aexit__(self, *_, **__):
        """Close connect with kafka producer."""
        await self.close_connections()

    async def on_before_send(self, topic, prepared_message) -> None:
        """Call after send."""

    async def on_after_send(self, topic, prepared_message) -> None:
        """Call after send."""

    def config_driver(self, *args, **kwargs) -> AsyncKafkaProducer:
        """Config driver."""
        self._params = {'args': args, 'kwargs': kwargs}
        return self

    async def init_connections(self) -> None:
        """Start kafka consumer."""
        self.producer = aiokafka.AIOKafkaProducer(*self._params['args'], **self._params['kwargs'])
        await self.producer.start()

    async def close_connections(self) -> None:
        """Close all connections."""
        logger.info("Closing connections...")
        if self.producer:
            await self.producer.stop()

    async def _basic_send(self, prepared_message: bytes, topic: str):
        """Send message in kafka."""

        await self.on_before_send(topic, prepared_message)
        try:
            await self.producer.send(topic, prepared_message)
            await self.on_after_send(topic, prepared_message)
        except KafkaTimeoutError:
            logger.exception('Produce timeout error...')
        except KafkaError:
            logger.exception('Kafka error on produce...')

    async def send_json(self, message: typing.Union[typing.Dict, typing.Iterable], topic: str) -> None:
        """Send json in kafka."""
        try:
            prepared_message: bytes = json.dumps(message)
        except json.JSONEncodeError:
            logger.exception(f'Type is not JSON serializable: {message}.')
            return
        except ValueError:
            logger.exception(f'Could not convert {message} to json.')
            return
        await self._basic_send(prepared_message, topic)

    async def send_raw(self, message: bytes, topic: str) -> None:
        """Send bytes in kafka."""
        await self._basic_send(message, topic)

    async def send_text(self, message: str, topic: str) -> None:
        """Send text in kafka."""
        try:
            encoded_message: bytes = message.encode()
        except UnicodeError:
            logger.exception('Unicode decoding error occurred')
            return None
        await self._basic_send(encoded_message, topic)
