"""Base kafka consumer."""
from __future__ import annotations
import asyncio
import logging
import signal
import traceback
import typing
from abc import ABC, abstractmethod

import aiokafka
import orjson
import pydantic
import tenacity
from loguru import logger


ConsumerMessage = typing.Union[bytes, str, typing.Iterable, typing.Dict, pydantic.main.ModelMetaclass]


class AsyncBaseConsumer(ABC):
    """Async base consumer."""

    encoder: typing.Union[str, pydantic.main.ModelMetaclass]  # also can be "text", "bytes" or "json"
    GRACEFUL_TIMEOUT: int = 10
    SIGNALS: typing.Tuple = (signal.SIGTERM, signal.SIGINT, signal.SIGQUIT)
    _params: typing.Dict

    def __init__(self, middleware: typing.Optional[typing.List[typing.Callable]] = None):
        """Init method."""
        self._main_task: typing.Optional[asyncio.Task] = None
        self._current_tasks: typing.Set[asyncio.Task] = set()
        self._middleware_stack: typing.List[
            typing.Callable[[ConsumerMessage], typing.Awaitable]
        ] = middleware if middleware else []

    @abstractmethod
    async def process_one_message(self, message: ConsumerMessage) -> None:
        """Process one message."""

    @abstractmethod
    async def process_multiple_messages(self) -> None:
        """Run process multiple messages forever until exception or cancel."""

    @abstractmethod
    async def init_connections(self) -> None:
        """Create connection."""

    @abstractmethod
    async def close_connections(self) -> None:
        """Close all connections."""

    async def on_before_start(self) -> None:
        """Call before start."""

    async def on_after_start(self) -> None:
        """Call after start."""

    async def on_before_close(self) -> None:
        """Call before close."""

    async def on_after_close(self) -> None:
        """Call after close."""

    def config_driver(self, *args, **kwargs) -> AsyncBaseConsumer:
        """Config driver."""
        self._params: typing.Dict = {'args': args, 'kwargs': kwargs}
        return self

    async def cancel_main_task(self) -> None:
        """Cancel task and close connections."""
        logger.info('Cancelling main task...')
        if self._main_task and not self._main_task.cancelled():
            self._main_task.cancel()

    def _finish_task(self, task: asyncio.Task):
        """Finish processing task."""
        exc: typing.Optional[BaseException] = task.exception()
        if exc:
            logger.error(
                f'Task {task} has failed with the exception:'
                f'{traceback.format_exception(type(exc), exc, exc.__traceback__)}'
            )
        self._current_tasks.discard(task)
        logger.debug(f'Task {task} has finished')

    def _create_task(self, coroutine: typing.Coroutine):
        """Create task for processing."""
        task: asyncio.Task = asyncio.create_task(coroutine)
        self._current_tasks.add(task)
        task.add_done_callback(self._finish_task)
        logger.debug(f'Task {task} has created')

    async def wait_for_subtasks(self) -> None:
        """Wait for completing subtasks before timeout."""
        logger.info('Gracefully waiting for tasks to end...')
        try:
            await asyncio.wait_for(asyncio.gather(*self._current_tasks), self.GRACEFUL_TIMEOUT)
        except asyncio.TimeoutError:
            logger.exception('Whoops, some tasks haven\'t finished in graceful time, sorry')

    async def prepare_message(self, body: bytes) -> typing.Optional[ConsumerMessage]:
        """Prepare message."""

        if self.encoder == 'bytes':
            return body

        try:
            decoded_body: str = body.decode()
        except UnicodeError:
            logger.exception('Unicode decoding error occurred')
            return None

        if self.encoder == 'text':
            return decoded_body

        try:
            message: typing.Union[typing.Iterable, typing.Dict] = orjson.loads(decoded_body)
        except orjson.JSONDecodeError:
            logger.exception(f'Json decode error occurred: {decoded_body}')
            return None
        except TypeError:
            logger.exception(f'The JSON object must be str, bytes or bytearray, not NoneType: {decoded_body}')
            return None

        if self.encoder == 'json':
            return message

        if isinstance(self.encoder, pydantic.main.ModelMetaclass):
            try:
                return self.encoder(**message)
            except pydantic.ValidationError:
                logger.exception(f'No necessary params in message {message}')
                return None

        return None

    async def callback(self, body: bytes) -> None:
        """Handle callback."""
        message: typing.Optional[ConsumerMessage] = await self.prepare_message(body)
        if message:
            # Vlad: i've double checked it, everything is really fine, but mypy fucks up here,
            # so i set type: ignore on these (if it's not fine - blame me)
            for middleware in self._middleware_stack:
                message = await middleware(message)  # type: ignore
            await self.process_one_message(message)  # type: ignore

    async def run_forever(self) -> None:
        """Run consumer forever until exception or cancel."""
        self._main_task = asyncio.create_task(self.process_multiple_messages())
        try:
            await self._main_task
        except asyncio.CancelledError:
            logger.info('Main task was cancelled')
            return None

    async def start(self) -> None:
        """Start consume."""
        signals: typing.Tuple[int, ...] = self.SIGNALS
        current_loop: asyncio.AbstractEventLoop = asyncio.get_running_loop()
        for _s in signals:
            current_loop.add_signal_handler(
                _s, lambda: asyncio.create_task(self.cancel_main_task()),
            )
        try:
            await self.on_before_start()
            await self.init_connections()
            await self.run_forever()
            await self.on_after_start()
        finally:
            await self.on_before_close()
            await self.wait_for_subtasks()
            await self.close_connections()
            await self.on_after_close()


class AsyncBaseKafkaConsumer(AsyncBaseConsumer):
    """Async base consumer for kafka."""

    TENACITY: typing.Dict = {'RETRY': {'TRIES': 3, 'DELAY': 1}}
    consumer: aiokafka.AIOKafkaConsumer

    async def close_connections(self) -> None:
        """Close all connections."""
        logger.info('Closing connections...')
        await super().close_connections()
        if self.consumer:
            await self.consumer.stop()

    async def init_connections(self) -> None:
        """Start kafka consumer."""
        self.consumer = aiokafka.AIOKafkaConsumer(*self._params['args'], **self._params['kwargs'])
        await self.consumer.start()

    async def process_multiple_messages(self) -> None:
        """Run process multiple messages forever until exception or cancel."""
        async for msg in self.consumer:
            self._create_task(self.callback(msg.value))

    @tenacity.retry(
        stop=tenacity.stop_after_attempt(TENACITY['RETRY']['TRIES']),
        wait=tenacity.wait_fixed(TENACITY['RETRY']['DELAY']),
        before=tenacity.before_log(logger, logging.DEBUG),
        retry=(
            tenacity.retry_if_exception_type(aiokafka.errors.RecordTooLargeError)
            | tenacity.retry_if_exception_type(aiokafka.errors.InvalidMessageError)
            | tenacity.retry_if_exception_type(ConnectionError)
            | tenacity.retry_if_exception_type(aiokafka.errors.KafkaTimeoutError)
            | tenacity.retry_if_exception_type(RuntimeError)
            | tenacity.retry_if_exception_type(aiokafka.errors.KafkaError)
        ),
    )
    async def run_forever(self) -> None:
        """Run consumer forever until exception or cancel."""
        await super().run_forever()
