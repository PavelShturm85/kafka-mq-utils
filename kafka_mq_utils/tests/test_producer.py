"""Producer tests."""
from unittest import mock

import orjson
import pytest

from chat_mq_utils.generic_producer import AsyncKafkaProducer, KafkaError, KafkaTimeoutError, json
from .mock import AIOKafkaProducerMock


async def basic_usage(message):
    """Basic usage."""
    producer: AsyncKafkaProducer = AsyncKafkaProducer().config_driver(
        bootstrap_servers="localhost:7777", enable_idempotence=True, compression_type="gzip"
    )
    await producer.init_connections()
    await producer.send_json(message, 'test_topic')
    await producer.send_raw('message'.encode(), 'test_topic')
    await producer.send_text('message', 'test_topic')
    await producer.close_connections()


async def context_manager_usage(message):
    """Context usage."""
    async with AsyncKafkaProducer().config_driver(
        bootstrap_servers='localhost:9092', enable_idempotence=True, compression_type="gzip"
    ) as produce:
        await produce.send_json(message, 'test_topic')
        await produce.send_raw('message'.encode(), 'test_topic')


@pytest.mark.asyncio
@pytest.mark.parametrize('dumps_error', [False, TypeError, ValueError, orjson.JSONEncodeError])
@pytest.mark.parametrize('produce_error', [False, KafkaTimeoutError, KafkaError])
@pytest.mark.parametrize(
    'message', [{"jwt": "some_jwt", "session_id": "some_id", "payload": dict()}, [{"test": "test"}]],
)
@pytest.mark.parametrize('test', [context_manager_usage, basic_usage])
async def test_kafka_producer(monkeypatch, dumps_error, produce_error, message, test):
    """Test kafka producer."""
    monkeypatch.setattr("aiokafka.AIOKafkaProducer", AIOKafkaProducerMock)
    if produce_error:
        monkeypatch.setattr("aiokafka.AIOKafkaProducer.send", mock.Mock(side_effect=produce_error))
    if dumps_error:
        monkeypatch.setattr(json, 'dumps', mock.Mock(side_effect=dumps_error))
    await test(message)
