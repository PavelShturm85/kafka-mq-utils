"""Mock chat mq utils."""
import asyncio
import time
import typing
from collections import namedtuple
from unittest import mock

import orjson as json
import pydantic


class SomeSerializer(pydantic.BaseModel):
    """Mock serializer."""

    jwt: str
    payload: typing.Dict


class BaseKafkaMock:
    """Base kafka mock."""

    def __init__(self, *_, **__):
        self.raisers = {}

    async def start(self) -> None:
        """Mock method start."""
        self._raise_if_need('start')

    async def stop(self) -> None:
        """Mock method stop."""
        self._raise_if_need('stop')

    def _raise_if_need(self, method: str):
        if self.raisers and method in self.raisers:
            raise self.raisers[method]


class AIOKafkaProducerMock(BaseKafkaMock):
    """Mock for AIOKafkaProducer."""

    async def send_and_wait(self, *_, **__) -> None:
        """Mock method send_and_wait."""
        self._raise_if_need('send_and_wait')
        await asyncio.sleep(0)

    async def send(self, *_, **__) -> None:
        """Mock method send."""
        self._raise_if_need('send')
        await asyncio.sleep(0)


class AIOKafkaConsumerMock(BaseKafkaMock):
    """Mock for AIOKafkaProducer."""

    def __init__(self, *_, **__):
        super().__init__(_, __)
        self.counter: int = 0
        self.iter_to: int = 5
        self.value: typing.Dict = {}
        self.callback_after_gen_stop: typing.Callable = mock.Mock()

    def __aiter__(self):
        return self

    async def commit(self):
        """Commit method."""

    async def __anext__(self):
        if self.counter >= self.iter_to:
            if self.callback_after_gen_stop:
                self.callback_after_gen_stop(self)
            self._raise_if_need('anext_stop')
            raise StopAsyncIteration
        self.counter += 1
        await asyncio.sleep(0)
        return self._generate_one_message()

    def _generate_one_message(self):
        """Generate message."""
        template_message: namedtuple = namedtuple(
            'message', ['topic', 'partition', 'offset', 'key', 'value', 'timestamp']
        )
        return template_message(
            **dict(
                topic='topic_test',
                partition='partition_test',
                offset='offset_test',
                key='key_test',
                value=json.dumps(self.value),
                timestamp=str(time.time()),
            )
        )
