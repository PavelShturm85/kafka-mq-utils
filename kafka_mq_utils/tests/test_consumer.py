"""Consumer tests."""
import asyncio
import typing
from unittest import mock

import orjson
import pydantic
import pytest

from chat_mq_utils import generic_consumer
from .mock import AIOKafkaConsumerMock, SomeSerializer


@pytest.mark.asyncio
@pytest.mark.parametrize(
    'asyncio_error', [False, True],
)
@pytest.mark.parametrize(
    'wait_for_error', [False, asyncio.TimeoutError],
)
@pytest.mark.parametrize(
    'loads_error', [False, TypeError, orjson.JSONDecodeError('test', 'test', 1)],
)
@pytest.mark.parametrize(
    'message',
    [
        {'body': {'jwt': 'some_jwt', 'payload': {}}, 'type': SomeSerializer},
        {'body': {'test': 'some_value', 'test_dict': {}}, 'type': SomeSerializer},
        {'body': {'jwt': 'some_jwt', 'payload': {}}, 'type': 'json'},
        {'body': 'test_bytes', 'type': 'bytes'},
        {'body': 'test_text', 'type': 'text'},
        {'body': 'test_text', 'type': 'test'},
    ],
)
async def test_kafka_consumer(monkeypatch, message, loads_error, asyncio_error, wait_for_error):
    """Test kafka consumer."""

    class ForTestAsyncBaseKafkaConsumer(generic_consumer.AsyncBaseKafkaConsumer):
        """For test AsyncBaseKafkaConsumer."""

        encoder: typing.Union[str, pydantic.main.ModelMetaclass] = message['type']

        async def process_one_message(
            self, message: typing.Union[bytes, str, typing.Dict, typing.Iterable, pydantic.main.ModelMetaclass]
        ) -> None:
            """Does the main task."""

    class AsyncKafkaConsumerWithCancelledError(ForTestAsyncBaseKafkaConsumer):
        """Mock consumer to raise cancelled error."""

        async def process_multiple_messages(self) -> None:
            """Run process multiple messages forever until exception or
            cancel."""
            await asyncio.sleep(0.3)
            raise asyncio.CancelledError()

    class CustomAIOKafkaConsumerMock(AIOKafkaConsumerMock):
        """Custom mock for AIOKafkaConsumer."""

        def __init__(self, *_, **__):
            super().__init__(_, __)
            self.value = message['body']

    consumer = ForTestAsyncBaseKafkaConsumer()
    if loads_error:
        monkeypatch.setattr(orjson, 'loads', mock.Mock(side_effect=loads_error))
    if asyncio_error:
        consumer = AsyncKafkaConsumerWithCancelledError()
    if wait_for_error:
        monkeypatch.setattr(asyncio, 'wait_for', mock.AsyncMock(side_effect=wait_for_error))

    monkeypatch.setattr("aiokafka.AIOKafkaConsumer", CustomAIOKafkaConsumerMock)
    consumer.config_driver(
        'some_topic', group_id='GROUP_ID', enable_auto_commit=True, bootstrap_servers='localhost:7777'
    )
    await consumer.start()

    if not any([wait_for_error, asyncio_error, loads_error]):
        await consumer.cancel_main_task()


@pytest.mark.asyncio
async def test_middleware():
    """Test middleware for working."""

    class ConsumerForTest(generic_consumer.AsyncBaseConsumer):
        """Test consumer."""

        encoder = 'json'

        async def process_one_message(
            self, message: typing.Union[bytes, str, typing.Dict, typing.Iterable, pydantic.main.ModelMetaclass]
        ) -> None:
            """Check if message has a new field."""
            assert message['test_field'] == 'test_value'  # type: ignore

        async def process_multiple_messages(self) -> None:
            """Just pass."""

        async def close_connections(self) -> None:
            """Just pass."""

        async def init_connections(self) -> None:
            """Just pass."""

    class MiddlewareForTest:
        """Test middleware."""

        async def __call__(self, message: typing.Dict):
            """Add new field to the message."""
            message['test_field'] = 'test_value'
            return message

    consumer_for_test = ConsumerForTest(middleware=[MiddlewareForTest()])
    await consumer_for_test.callback(orjson.dumps({'kek': 1}))
